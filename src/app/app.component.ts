import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-first-project';
  result!:number;
    Calculate(val1:string,val2:string,val3:string,val4:string){
      console.warn(val1,val2,val3,val4)
      this.result=((parseFloat(val1)*parseFloat(val2))/(parseFloat(val3)*100))+parseFloat(val4)+(0.01/parseFloat(val3));
    }
}
